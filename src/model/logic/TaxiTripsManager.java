package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Item;
import model.data_structures.List;
import model.data_structures.Node;
import model.data_structures.Pila;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager<T> implements ITaxiTripsManager {

	private List listaNodos;
	
	private Cola<Service> colaServicios;
	
	private Pila<Service> pilaServicios;
	
	public void loadServices(String serviceFile, String taxiId) 
	{
		colaServicios = new Cola<Service>();
		pilaServicios = new Pila<Service>();
		try 
		{
			BufferedReader bfr = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Item[] item = gson.fromJson(bfr, Item[].class);
			for(int i = 0; i < item.length;i++)
			{
				if(item[i].getTaxi_id().equals(taxiId))
				{
					colaServicios.enqueue(new Service(item[i].getTrip_start_timestamp()));
					pilaServicios.push(new Service(item[i].getTrip_start_timestamp()));
				}
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("---------------Ha ocurrido un problema---------------\n");
		}
	}

	@Override
	public int [] servicesInInverseOrder() 
	{
		int [] resultado = new int[2];
		Service servicioA = pilaServicios.pop();
		Service servicioB = pilaServicios.pop();
		while(pilaServicios.getSize() != 0)
		{
			if(servicioB != null && servicioA.compareTo(servicioB)>=0)
			{
				resultado[0]++;
			}
			else
			{
				resultado[1]++;
			}
			servicioA = servicioB;
			servicioB = pilaServicios.pop();
		}
		
		return resultado;
	}

	@Override
	public int [] servicesInOrder() 
	{
		int [] resultado = new int[2];
		Service servicioA = colaServicios.dequeue();
		Service servicioB = colaServicios.dequeue();
		while(colaServicios.getSize() != 0)
		{
			System.out.println(servicioA.getTripStartTime());
			if(servicioB != null && servicioA.compareTo(servicioB)>=0)
			{
				resultado[0]++;
			}
			else
			{
				resultado[1]++;
			}
			servicioA = servicioB;
			servicioB = colaServicios.dequeue();
		}
		
		return resultado;
	}
}
