package model.data_structures;

public interface LinkedList <T> 
{
	/**
	 * A�ade un nodo al final de la lista.
	 * @param node Node que se desea agregar.
	 */
	public void add(T newItem);
	
	/**
	 * Retorna el nodo en la posicion pasada por parametro.
	 * @param pIndex Int con la posicion del nodo
	 * @return Node en la posicion de parametro.
	 */
	public T get(int pIndex);
	
	/**
	 * Retorna el tama�o de la lista.
	 * @return Int con el tama�o de la lista.
	 */
	public int size();
	
	/**
	 * Retorna el primer nodo de la lista.
	 * @return Node en la primera posicion de la lista.
	 */
	public Node getFirst();
	
	/**
	 * A�ade un nodo con un taxi al final de la lista.
	 * @param node Node con el taxi que se agrega a la lista
	 */
	public void addTaxi(T item);
}
