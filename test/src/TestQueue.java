

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.TestCase;
import model.data_structures.Cola;
import model.data_structures.Item;
import model.data_structures.List;

public class TestQueue<T> extends TestCase
{
	/**
	 * String con la direcci�n del archivo de test.
	 */
	private String serviceFile = "./data/test/taxi-trips-wrvz-psew-subset-test.json";

	/**
	 * Lista principal de la estructura de datos.
	 */
	private List listaNodos = new List();
	
	/**
	 * Pila para la prueba
	 */
	private Cola cola = new Cola();

	/**
	 * Carga los datos del Json a la lista principal de datos
	 */
	public void setupEscenario1()
	{
		try 
		{
			BufferedReader bfr = new BufferedReader( new FileReader(""));
			fail("Debi� generar error");
		} 
		catch (FileNotFoundException e) 
		{
		}
		try
		{
			BufferedReader bfr = new BufferedReader( new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Item[] item = gson.fromJson(bfr, Item[].class);
			for(int i = 0; i < item.length;i++)
			{
				T newItem = (T) item[i];
				listaNodos.add(newItem);
			}

		}
		catch(Exception e)
		{
			fail("No debi� generar error");
		}
	}
	
	/**
	 * Testea el enqueue de la cola.
	 */

	public void testEnqueue()
	{
		setupEscenario1();
		cola.enqueue(listaNodos.get(0));
		assertEquals("No era el nodo esperado", "807480408dc85f9a8b8db86ad7b7107b11a52a30", ((Item)cola.dequeue()).getTrip_id());
		
		for(int i = 0; i < listaNodos.size() ; i++)
		{
			cola.enqueue(listaNodos.get(i));
		}
		cola.dequeue();
		cola.dequeue();
		assertEquals("No era el nodo esperado", "8162919369989b53fb2ab77218c0508d0b9eb562", ((Item)cola.dequeue()).getTrip_id());
	}
	
	/**
	 * Testea el dequeue de la cola.
	 */

	public void testDequeue()
	{
		setupEscenario1();
		for(int i = 0; i < listaNodos.size() ; i++)
		{
			cola.enqueue(listaNodos.get(i));
		}
		cola.dequeue();
		assertEquals("No era el nodo esperado", "8143acd1493d068b08121e23fbda8cb37377157e", ((Item)cola.dequeue()).getTrip_id());
		for(int i = 0; i < 5 ; i++)
		{
			cola.dequeue();
		}
		assertEquals("No era el nodo esperado", "8330dcb66948c55a8ccd352f9a41aca48a79aade", ((Item)cola.dequeue()).getTrip_id());
	}
	
	/**
	 * Testea el getSize de la cola.
	 */

	public void testGetSize()
	{
		assertEquals("No era el valor esperado", 0, cola.getSize());
		
		setupEscenario1();
		for(int i = 0; i < 5 ; i++)
		{
			cola.enqueue(listaNodos.get(i));
		}
		assertEquals("No era el valor esperado", 5, cola.getSize());
		
		for(int i = 0; i < listaNodos.size() ; i++)
		{
			cola.enqueue(listaNodos.get(i));
		}
		assertEquals("No era el valor esperado", 25, cola.getSize());
	}
	
	/**
	 * Testea el isEmpty de la cola.
	 */

	public void testIsEmpty()
	{
		assertEquals("Se esperaba true", true, cola.isEmpty());
		
		setupEscenario1();
		for(int i = 0; i < 5 ; i++)
		{
			cola.enqueue(listaNodos.get(i));
		}
		assertEquals("No esperaba false", false, cola.isEmpty());
	}
}
