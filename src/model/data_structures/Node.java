package model.data_structures;

public class Node<T> 
{
	/**
	 * Siguiente nodo en la lista.
	 */
	private Node<T> next;
	
	/**
	 * Item con la informacion del nodo.
	 */
	private T item; 

	/**
	 * Crea un nodo con un item que llega por parametro.
	 * @param item Item con la información del nodo.
	 */
	public Node (T item) 
	{ 
		next = null; 
		this.item = item;
	} 
	
	/**
	 * Retorna el siguiente nodo en la lista.
	 * @return Node nodo siguiente en la lista.
	 */
	public Node<T> getNext() 
	{ 
		return next;
	} 

	/**
	 * Adigna el siguiente nodo en la lista.
	 * @param next Siguiente nodo en la lista.
	 */
	public void setNextNode ( Node<T> next) 
	{
		this.next = next;
	}

	/**
	 * Retorna el item del nodo con la información.
	 * @return Item con la información.
	 */
	public T getItem()
	{ 
		return item;
	}

	/**
	 * Asigna el Item del nodo por el que llega por parametro.
	 * @param item Item que se le asigna al nodo.
	 */
	public void setItem (T item) 
	{ 
		this.item = item;
	}
	
	/**
	 * Valida si tiene siguiente nodo el nodo actual.
	 * @return True si tiene siguiente nodo, false de lo contrario.
	 */
	public boolean hasNext()
	{
		return (next == null)?false : true;
	}

}
