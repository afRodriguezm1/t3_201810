package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private Date trip_start_timestamp;
	
	public Service(Date fecha)
	{
		trip_start_timestamp = fecha;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public Date getTripStartTime() {
		// TODO Auto-generated method stub
		return trip_start_timestamp;
	}

	@Override
	public int compareTo(Service o) 
	{
		int horasThis = trip_start_timestamp.getHours();
		int horasParam = o.getTripStartTime().getHours();
		
		if(horasThis==horasParam)
		{
			int minutosThis = trip_start_timestamp.getMinutes();
			int minutosParam = o.getTripStartTime().getMinutes();
			
			if(minutosThis == minutosParam)
			{
				int segundosThis = trip_start_timestamp.getSeconds();
				int segundosParam = o.getTripStartTime().getSeconds();
				
				if(segundosThis == segundosParam)
					return 0; // cuando las horas son exactamente iguales.
				else if(segundosThis > segundosParam)
					return -1; // cuando la hora que llega por parametro es menor.
				else
					return 1; // cuando la hora que llega por parametro es mayor.
			}
			else if( minutosThis > minutosParam)
				return -1; // cuando la hora que llega por parametro es menor.
			else
				return 1; // cuando la hora que llega por parametro es mayor.
		}
		else if(horasThis > horasParam)
			return -1; // cuando la hora que llega por parametro es menor.
		
		return 1; // cuando la hora que llega por parametro es mayor.
	}
}
