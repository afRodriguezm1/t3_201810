package model.data_structures;


public class List <T> implements LinkedList<T>
{
	/**
	 * Primer nodo en la lista.
	 */
	private Node<T> head;
	
	/**
	 * Ultimo nodo en la lista.
	 */
	private Node<T> tail;
	
	/**
	 * Tama�o de la lista.
	 */
	private int size;
	
	/**
	 * Agrega un nodo con Item de ultimo en la lista.
	 */
	public void add(T newItem)
	{			

		Node node = new Node(newItem);
		if(head == null)
		{
			head = node;
			tail = node;
			size ++;
		}
		else
		{
			tail.setNextNode(node);
			tail = node;
			size++;
		}
	}
	
	/**
	 * Retorna el nodo en la poscicion que llega por parametro.
	 * @param pIndex int con la posicion del nodo que se busca.
	 * @return Node nodo en la posicion por parametro.
	 */
	public T get(int pIndex)
	{
		if(head == null || pIndex == size)
			return null;
		else
		{
			int pos = 0;
			Node<T> nodo = head;
			while(nodo != null && pos < pIndex )
			{
				nodo = nodo.getNext();
				pos ++;
			}
			return nodo.getItem();
		}
	}
	
	public void addTaxi(T item)
	{
//		if(head == null)
//		{
//			head = node;
//			tail = node;
//			size ++;
//		}
//		else
//		{
//			Taxi newTaxi = (Taxi)node.getItem();
//			Node nodeAct = head;
//			while(nodeAct != null)
//			{
//				if(((Taxi) nodeAct.getItem()).compareTo(newTaxi)==0)
//				{
//					break;
//				}
//				nodeAct = nodeAct.getNext();
//			}
//			
//			tail.setNextNode(node);
//			tail = node;
//			size++;
//		}
	}
	
	/**
	 * Retorna el tama�o de la lista.
	 * @return int con el tama�o de la lista.
	 */
	public int size()
	{
		return size;
	}
	
	/**
	 * Retorna el primer nodo de la lista.
	 * @return Node en la primera poscicion.
	 */
	public Node getFirst()
	{
		return head;
	}
}
