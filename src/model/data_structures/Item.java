package model.data_structures;

import java.awt.Point;
import java.util.Date;

public class Item 
{
	/**
	 * Id de la carrera.
	 */
	private String trip_id;

	/**
	 * Id del taxi.
	 */
	private String taxi_id;
	
	/**
	 * Fecha y Hora de inicio de la carrera.
	 */
	private Date trip_start_timestamp;

	/**
	 * Fecha y hora de fin de la carrera.
	 */
	private Date trip_end_timestamp;

	/**
	 * Tiempo en segundos de la carrera.
	 */
	private int trip_seconds;

	/**
	 * Millas de la carrera.
	 */
	private float trip_miles;

	/**
	 * The Census Tract where the trip began.
	 */
	private String pickup_census_tract;

	/**
	 * The Census Tract where the trip ends.
	 */
	private String dropoff_census_tract;

	/**
	 * C�digo del area de inicio de la carrera.
	 */
	private int pickup_community_area;

	/**
	 * C�digo del area de fin de la carrera.
	 */
	private int dropoff_community_area;
	
	/**
	 * Tarifas adicionales de la carrera.
	 */
	private float fare;
	
	/**
	 * Propinas al finalizar la carrera.
	 */
	private float tips;
	
	/**
	 * Costo peajes durante la carrera.
	 */
	private float tolls;
	
	/**
	 * Extras de la carrera.
	 */
	private float extras;
	
	/**
	 * Costo total de la carrera.
	 */
	private float trip_total;
	
	/**
	 * Tipo de pago de la carrera.
	 */
	private String payment_type;
	
	/**
	 * Compa�ia del taxi.
	 */
	private String company;
	
	/**
	 * Latitud del lugar de inicio de la carrera.
	 */
	private double pickup_centroid_latitude;
	
	/**
	 * Longitud del lugar de inicio de la carrera.
	 */
	private double pickup_centroid_longitude;
	
	/**
	 * Localizacion del punto de inicio de la carrera.
	 */
	private Point pickup_centroid_location;

	/**
	 * Latitud del lugar de fin de la carrera.
	 */
	private double dropoff_centroid_latitude;

	/**
	 * Longitud del lugar de fin de la carrera.
	 */
	private double dropoff_centroid_longitude;

	/**
	 * Localizacion del punto de inicio de la carrera.
	 */
	private Point dropoff_centroid_location;
	
	/**
	 * Retorna la Id de la carrera. 
	 * @return trip_id de la carrera.
	 */
	public String getTrip_id() 
	{
		return trip_id;
	}

	/**
	 * Retorna la Id del taxi.
	 * @return taxi_id de la carrera.
	 */
	public String getTaxi_id() 
	{
		return taxi_id;
	}

	/**
	 * Retorna la fecha y hora de inicio de la carrera.
	 * @return trip_start_timestamp de la carrera.
	 */
	public Date getTrip_start_timestamp() 
	{
		return trip_start_timestamp;
	}

	/**
	 * Retorna la fecha y hora de fin de la carrera.
	 * @return trip_end_timestamp de la carrera.
	 */
	public Date getTrip_end_timestamp() 
	{
		return trip_end_timestamp;
	}
	
	/**
	 * Retorna el tiempo en segundos de la carrera.
	 * @return trip_seconds de la carrera.
	 */
	public int getTrip_seconds() 
	{
		return trip_seconds;
	}

	/**
	 * Retorna las millas recorridas en la carrera.
	 * @return trip_miles de la carrera.
	 */
	public float getTrip_miles() 
	{
		return trip_miles;
	}

	/**
	 * Retorna el census tract de inicio de la carrera.
	 * @return pickup_census_tract de la carrera.
	 */
	public String getPickup_census_tract() 
	{
		return pickup_census_tract;
	}

	/**
	 * Retorna el census tract de fin de la carrera.
	 * @return dropoff_census_tract.
	 */
	public String getDropoff_census_tract() 
	{
		return dropoff_census_tract;
	}

	/**
	 * Retorna el area donde inici� la carrera.
	 * @return pickup_community_area de la carrera.
	 */
	public int getPickup_community_area() 
	{
		return pickup_community_area;
	}
	
	/**
	 * Retorna el area donde finaliz� la carrera.
	 * @return dropoff_community_area de la carrera.
	 */
	public int getDropoff_community_area() 
	{
		return dropoff_community_area;
	}

	/**
	 * Retorna las tarifas adicionales de la carrera.
	 * @return fare de la carrera.
	 */
	public float getFare() 
	{
		return fare;
	}

	/**
	 * Retorna las propinas de la carrera.
	 * @return tips de la carrera.
	 */
	public float getTips() 
	{
		return tips;
	}

	/**
	 * Retorna el costo de peajes de la carrera.
	 * @return tolls de la carrera.
	 */
	public float getTolls() 
	{
		return tolls;
	}

	/**
	 * Retorna los extras de la carrera.
	 * @return extras de la carrera.
	 */
	public float getExtras() 
	{
		return extras;
	}

	/**
	 * Retorna el precio total de la carrera.
	 * @return trip_total de la carrera.
	 */
	public float getTrip_total() 
	{
		return trip_total;
	}

	/**
	 * Retorna el tipo de pago de la carrera.
	 * @return payment_type de la carrera.
	 */
	public String getPayment_type() 
	{
		return payment_type;
	}

	/**
	 * Retorna la compa�ia del taxi de la carrera.
	 * @return company del taxi de la carrera.
	 */
	public String getCompany() 
	{
		return company;
	}

	/**
	 * Retorna la latitud de inicio de la carrera.
	 * @return pickup_centroid_latitude de la carrera.
	 */
	public double getPickup_centroid_latitude() 
	{
		return pickup_centroid_latitude;
	}

	/**
	 * Retorna la longitud de inicio de la carrera.
	 * @return pickup_centroid_longitude de la carrera.
	 */
	public double getPickup_centroid_longitude() 
	{
		return pickup_centroid_longitude;
	}

	/**
	 * Retorna el punto de inicio de la carrera.
	 * @return pickup_centroid_location de la carrera.
	 */
	public Point getPickup_centroid_location() 
	{
		return pickup_centroid_location;
	}

	/**
	 * Retorna la latitud de fin de la carrera.
	 * @return dropoff_centroid_latitude de la carrera.
	 */
	public double getDropoff_centroid_latitude() 
	{
		return dropoff_centroid_latitude;
	}

	/**
	 * Return longitud de fin de la carrera.
	 * @return dropoff_centroid_lungitude de la carrera.
	 */
	public double getDropoff_centroid_longitude() 
	{
		return dropoff_centroid_longitude;
	}

	/**
	 * Retorna el punto de fin de la carrera.
	 * @return dropoff_centroid_location de la carrera.
	 */
	public Point getDropoff_centroidL_location() 
	{
		return dropoff_centroid_location;
	}
}