import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.TestCase;
import model.data_structures.Item;
import model.data_structures.List;
import model.data_structures.Pila;

public class TestStack<T> extends TestCase
{
	/**
	 * String con la direcci�n del archivo de test.
	 */
	private String serviceFile = "./data/test/taxi-trips-wrvz-psew-subset-test.json";

	/**
	 * Lista principal de la estructura de datos.
	 */
	private List listaNodos = new List();
	
	/**
	 * Pila para la prueba
	 */
	private Pila pila = new Pila();

	/**
	 * Carga los datos del Json en la lista de datos principal.
	 */
	public void setupEscenario1()
	{
		try 
		{
			BufferedReader bfr = new BufferedReader( new FileReader(""));
			fail("Debi� generar error");
		} 
		catch (FileNotFoundException e) 
		{
		}
		try
		{
			BufferedReader bfr = new BufferedReader( new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Item[] item = gson.fromJson(bfr, Item[].class);
			for(int i = 0; i < item.length;i++)
			{
				T newItem = (T) item[i];
				listaNodos.add(newItem);
			}

		}
		catch(Exception e)
		{
			fail("No debi� generar error");
		}
	}
	
	/**
	 * Testea el m�todo push de la pila.
	 */
	public void testPush()
	{
		setupEscenario1();
		pila.push(listaNodos.get(0));
		assertEquals("Debio agregar el nodo adecuadamente", "807480408dc85f9a8b8db86ad7b7107b11a52a30",((Item)pila.pop()).getTrip_id());
		
		pila.push(listaNodos.get(0));
		pila.push(listaNodos.get(1));
		pila.push(listaNodos.get(2));
		assertEquals("Debio agregar el nodo adecuadamente", "8162919369989b53fb2ab77218c0508d0b9eb562",((Item)pila.pop()).getTrip_id());
	}
	
	/**
	 * Testea el m�todo pop de la pila.
	 */
	public void testPop()
	{
		setupEscenario1();
		pila.push(listaNodos.get(0));
		pila.push(listaNodos.get(1));
		pila.push(listaNodos.get(2));
		assertEquals("Debio agregar el nodo adecuadamente", "8162919369989b53fb2ab77218c0508d0b9eb562",((Item)pila.pop()).getTrip_id());
		assertEquals("Debio agregar el nodo adecuadamente", "8143acd1493d068b08121e23fbda8cb37377157e",((Item)pila.pop()).getTrip_id());
		assertEquals("Debio agregar el nodo adecuadamente", "807480408dc85f9a8b8db86ad7b7107b11a52a30",((Item)pila.pop()).getTrip_id());
	}
	
	/**
	 * Testea el m�todo getSize de la pila.
	 */
	public void testGetSize()
	{
		setupEscenario1();
		for(int i = 0; i < listaNodos.size() ; i++)
		{
			pila.push(listaNodos.get(i));
		}
		assertEquals("No es el valor esperado", 20, pila.getSize());
		
		setupEscenario1();
		for(int i = 0; i < 5 ; i++)
		{
			pila.push(listaNodos.get(i));
		}
		assertEquals("No es el valor esperado", 25, pila.getSize());
	}
	
	/**
	 * Testea el m�todo isEmpty de la pila.
	 */
	public void testIsEmpty()
	{
		assertEquals("Debio ser true", true, pila.isEmpty());
		
		setupEscenario1();
		pila.push(listaNodos.get(0));
		assertEquals("Debio ser false", false, pila.isEmpty());
	}
}
